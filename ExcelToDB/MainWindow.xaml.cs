﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WindForms = System.Windows.Forms;

namespace ExcelToDB
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Excel ex;
        List<string> filesName = new List<string>();
        public MainWindow()
        {
            InitializeComponent();
            button3.IsEnabled = false;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            WindForms.FolderBrowserDialog dlg = new WindForms.FolderBrowserDialog();

            WindForms.DialogResult result = dlg.ShowDialog();

            if (result == WindForms.DialogResult.OK)
            {
                textBox.Text = dlg.SelectedPath;
                filesName.Clear();
                filesName.AddRange(getFiles(dlg.SelectedPath));
            }
        }
        private List<string> getFiles(string dirName)
        {
            var fileList = new List<string>();
            DirectoryInfo dir = new DirectoryInfo(dirName);
            foreach (FileInfo files in dir.GetFiles("*.xls"))
            {
                fileList.Add(files.FullName);
            }
            return fileList;
        }
        ///<summary>
        ///запускает весь процесс обработки
        ///</summary>
        private async void button1_Click(object sender, RoutedEventArgs e)
        {
            labelProcess.Content = "Обработка данных...";
            await Task.Run(()=> {
                ex = new Excel(filesName);
                ex.RegisterHandler(new Excel.FileHandler(setProcLabel));
                ex.addDate();
                labelProcess.Content = "Обработка завершена";
            });
            button3.IsEnabled = false;
        }

        private async void button2_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(() => {
                ex = new Excel(filesName, true);
                ex.RegisterHandler(new Excel.FileHandler(setProcLabel));
                labelProcess.Content = "Обработка завершена";
                button3.IsEnabled = true;
            });
        }

        private void setProcLabel(string fileName)
        {
            labelProcess.Content = "Обработка" + fileName;
        }

        private async void button3_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(() => ex.addDate());
            labelProcess.Content = "Обработка завершена";
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Multiselect = true;
            dlg.Filter = "Excel Files|*.xls";
            if (dlg.ShowDialog() == true)
            {
                filesName.Clear();
                textBox.Text = dlg.FileName;
                foreach (string file in dlg.FileNames)
                {
                    filesName.Add(file);
                }
            }
            
        }
    }
}
