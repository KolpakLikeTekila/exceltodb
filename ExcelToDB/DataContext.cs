﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ExcelToDB.Entity;

namespace ExcelToDB
{
    class DataContext:DbContext
    {
        public DataContext(): base("Detail") { }

        public DbSet<DetailInfo> Details { get; set; }
        public DbSet<DateLastRepair> DateLastRepairs { get; set; }
        
        public DbSet<Body> Bodies { get; set; }
        public DbSet<DateMount> DateMounts { get; set; }
        public DbSet<DateYear> DateYears { get; set; }
        public DbSet<CharacterEquip> CharacterEquips { get; set; }
        public DbSet<NameEquip> NameEquips { get; set; }
        public DbSet<MarkRep> MarkReps { get; set; }

    }
}
