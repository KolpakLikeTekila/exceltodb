﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToDB.Entity
{
    class DateLastRepair
    {
        [Key]
        [ForeignKey("DetailInfo")]
        public int Id { get; set; }

        public DetailInfo DetailInfo { get; set; }

        public string Cur { get; set; }
        public string Cap { get; set; }

    }
}
