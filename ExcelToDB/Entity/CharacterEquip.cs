﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToDB.Entity
{
    class CharacterEquip
    {
        [Key]
        [ForeignKey("DetailInfo")]
        public int Id { get; set; }

        public double? Pow { get; set; }
        public double? Voltage { get; set; }
        public string Name { get; set;}

        public DetailInfo DetailInfo { get; set; }
    }
}
