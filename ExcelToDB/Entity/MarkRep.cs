﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToDB.Entity
{
    class MarkRep
    {
        [Key]
        [ForeignKey("DetailInfo")]
        public int Id { get; set; }
        public DetailInfo DetailInfo { get; set; }

        public bool? Cap { get; set; }
        public bool? Cur { get; set; }
        public bool? Right { get; set; }
        public bool? Tes { get; set; }

    }
}
