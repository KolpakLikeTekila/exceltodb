﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToDB.Entity
{
    class DateYear
    {
        [Key]
        [ForeignKey("DetailInfo")]
        public int Id { get; set; }
        public DetailInfo DetailInfo { get; set; }

        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Ju { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sept { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
    }
}
