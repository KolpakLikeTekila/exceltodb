﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToDB.Entity
{
    class DetailInfo :Detail
    {
        public NameEquip Name { get; set; }
        public CharacterEquip Character { get; set; }

        public DateYear DateYear { get; set; }
        public MarkRep Result { get; set; }
        public string Note { get; set; }

        public DateMount DateMount { get; set; }
        public string YearExc { get; set; }
    }
}
