﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToDB.Entity
{
    class Body
    {
        public int Id { get; set; }
        public string TechPos { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DetailInfo> Details { get; set; } = new List<DetailInfo>();

    }
}
