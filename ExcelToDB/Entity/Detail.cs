﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToDB.Entity
{
    class Detail
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string TechPos { get; set; }
        public string FreqRep { get; set; }
        public double LabRep { get; set; }
        public DateLastRepair DateLastRep { get; set; }

        public int TypeDetail { get; set; }
        public int TypeTime { get; set; }

        public int BodyId { get; set; }
        public Body Body { get; set; }
    }

    enum Time
    {
        Annual = 1,
        Mountly = 2
    }

    enum DetailType
    {
        Motor = 1,
        Equip = 2
    }
}
