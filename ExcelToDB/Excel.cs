﻿using ExcelToDB.Entity;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Drawing;

namespace ExcelToDB
{
    class Excel
    {
        public delegate void FileHandler(string fileName);
        FileHandler del;

        public void RegisterHandler(FileHandler _del)
        {
            del = _del;
        }
        //список всех экслек
        private List<string> _fileList;
        //список всех корпусов/позиций
        private List<Body> _detaileList = new List<Body>();
        private bool _cheackDate = false;
        //файл содержит в себе данные о электродвигателе
        bool _motor = false;
        //файл содержит в себе данные за год
        bool _annualSched = false;

        Dictionary<int, XlRgbColor> _colorCell = new Dictionary<int, XlRgbColor>();

        public Excel(List <string> files, bool cheackDate = false)
        {
            _fileList = files;
            _cheackDate = cheackDate;
            _fileList.ForEach(m => readFile(m));
        }

        public void addDate()
        {
            foreach (Body det in _detaileList)
            {
                DateB.addData(det);
            }
        }

        /// <summary>
        /// инициализирует список файлов эксель
        /// </summary>
        /// <param name="path"> путь до папки с экселями</param>
        private void getFilesName(string path)
        {
            _fileList = new List<string>();
            DirectoryInfo dir = new DirectoryInfo(path);

            foreach (FileInfo files in dir.GetFiles("*.xls"))
            {
                _fileList.Add(files.FullName);
            }
        }
        /// <summary>
        /// возаращет строку начала шапки и опредеят тип объектов в файле
        /// </summary>
        /// <param name="valueArray">матрица</param>
        /// <returns></returns>
        private int findTopTableAndCreateDecor(object[,] valueArray)
        {
            int row = 0;
            _annualSched = false;
            _motor = false;
            for (row = 1; row < valueArray.GetLength(0); ++row)
            {
                for (int col = 1; col <= valueArray.GetLength(1); ++col)
                {
                    if ((valueArray[row, col] != null && valueArray[row, col].ToString().Contains("ГОДОВОЙ")))
                    {
                        _annualSched = true;
                    }
                    if ((valueArray[row, col] != null && valueArray[row, col].ToString().Contains("электродвигателей")))
                    {
                        _motor = true;
                    }
                    if (valueArray[row, col] != null && valueArray[row, col + 1] != null)
                    {
                        return row;
                    }
                }
            }
            return row;
        }

        /// <summary>
        /// определяет номер строки, с которой начинаются данные
        /// </summary>
        /// <param name="valueArray">матрица</param>
        /// <returns></returns>
        private int startDate(object[,] valueArray)
        {
            int row = 0;
            for (row = findTopTableAndCreateDecor(valueArray) + 1; row < valueArray.GetLength(0); ++row)
            {
                if (valueArray[row, 1] != null && valueArray[row, 2] != null)
                {
                    return row + 1;
                }
            }
            return row;
        }

        /// <summary>
        /// возврщает строку конца таблицы
        /// </summary>
        /// <param name="startDate">строка начала данных в таблице</param>
        /// <param name="valueArray">матрица</param>
        /// <returns></returns>
        private int endDate(int startDate, object[,] valueArray)
        {
            int row = 0;
            for (row = startDate; row < valueArray.GetLength(0) - 1; ++row)
            {
                if (valueArray[row, 1] == null && valueArray[row, 2] == null && valueArray[row + 1, 1] == null && valueArray[row + 1, 2] == null)
                {
                    return row;
                }
            }
            return row;
        }

        /// <summary>
        /// обробатывает список
        /// </summary>
        /// <param name="valueArray">матрица</param>
        private void readDataList(object[,] valueArray)
        {
            if (valueArray != null)
            {
                Body body = new Body { };
                int start = startDate(valueArray);
                int end = endDate(start, valueArray);
                for (int row = start; row < end; ++row)
                {
                    if (valueArray[row, 1] == null && valueArray[row, 2] != null)
                    {
                        if (!string.IsNullOrEmpty(body.TechPos))
                        {
                            _detaileList.Add(body);
                        }
                        body = new Body { TechPos = valueArray[row, 2].ToString() };
                        if (valueArray[row, 3] != null)
                        {
                            body.Name = valueArray[row, 3].ToString();
                        }
                    }
                    else if (valueArray[row, 1] != null)
                    {
                        DetailInfo detail = readDetail2(row, valueArray);
                        detail.Body = body;
                        if ( _cheackDate){
                            if (DateB.CheckAddDate(detail))
                            {
                                _colorCell.Add(row, XlRgbColor.rgbGreen);
                                body.Details.Add(detail);
                            }
                            else
                            {
                                _colorCell.Add(row, XlRgbColor.rgbRed);
                            }
                        }
                        else
                        body.Details.Add(detail);
                    }
                }
                _detaileList.Add(body);
            }
        }

        private void ChangCellColor(Worksheet ws)
        {
            int endCellCol = _annualSched ? 24 : 41;
            foreach (var keyValue in _colorCell)
            {

                var startCell = (Range)ws.Cells[keyValue.Key, 1];
                var endCell = (Range)ws.Cells[keyValue.Key, endCellCol];
                var writeRange = ws.Range[startCell, endCell];

                writeRange.Interior.Color = keyValue.Value;
            }
        } 
        /// <summary>
        /// обробатывает файл
        /// </summary>
        /// <param name="fileNamae">имя файла</param>
        private void readFile(string fileNamae)
        {
            var xlApp = new Application();

            //получает объект эксель
            var xlWorkbook = xlApp.Workbooks.Open(Path.GetFullPath(fileNamae));

            del(fileNamae);

            //листы из экселя
            var xlWorksheet = xlWorkbook.Sheets;

            for (int i = 1; i <= xlWorkbook.Sheets.Count; i++)
            {
                Worksheet xlWorksheet1 = (Worksheet)xlWorkbook.Sheets.get_Item(i);
                Range xlRange = xlWorksheet1.UsedRange;
                readDataList((object[,])xlRange.get_Value(XlRangeValueDataType.xlRangeValueDefault));
                if (_cheackDate)
                {
                    ChangCellColor(xlWorksheet1);
                    _colorCell.Clear();
                }
                Marshal.ReleaseComObject(xlWorksheet1);
            }
            /*      xlWorkbook.Save();
                  // Garbage collecting
                  
                  // Clean up references to all COM objects
                  // As per above, you're just using a Workbook and Excel Application instance, so release them:
                  xlWorkbook.Close(false);
                  xlApp.Quit();
                  Marshal.FinalReleaseComObject(xlWorkbook);
                  Marshal.FinalReleaseComObject(xlApp);*/

            if (_cheackDate)
            {
                xlWorkbook.Close(true);
            }
            else
            xlWorkbook.Close();

            Marshal.ReleaseComObject(xlWorksheet);
            Marshal.ReleaseComObject(xlWorkbook);
            Marshal.ReleaseComObject(xlApp);
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private double[] getCharacter(string str)
        {
            string[] strChar = Regex.Split(str, "[а-яА-Я]");
            double[] mas = new double[2];
            int i = 0;
            foreach (string number in strChar)
            {
                if (!String.IsNullOrEmpty(number))
                {
                    mas[i++] = Convert.ToDouble(number.Replace('.', ','));
                }
            }
            return mas;
        }
/*
        #region Dinamic read Date
        private Detail readDetail(int row, object[,] valueArray)
        {
            Detail detail;

            DateLastRepair dtLastRep = new DateLastRepair { Cur = valueArray[row, 7].ToString(), Cap = valueArray[row, 8].ToString() };

            if (motor)
            {
                detail = new Motor { Number = valueArray[row, 1].ToString(), TechPos = valueArray[row, 2].ToString() };
                dtLastRep.Id = detail.Id;
                detail.DateLastRep = dtLastRep;

                string[] str = valueArray[row, 3].ToString().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                double speed = 0;
                double.TryParse(Regex.Split(str[str.Length - 2], "[а-яА-Я]")[0], out speed);
                NameMotor nmMotor = new NameMotor { Id = detail.Id, Speed = speed, Type = str[0] };
                (detail as Motor).Name = nmMotor;

                double[] character = getCharacter(valueArray[row, 4].ToString());//.Split(new[] { ' ', "[a - zA - Z] +" }, StringSplitOptions.RemoveEmptyEntries);
                CharacterMotor charMotor = new CharacterMotor { Id = detail.Id, Pow = Convert.ToDouble(character[0]), Voltage = Convert.ToDouble(character[1]) };
                (detail as Motor).Character = charMotor;
            }
            else
            {
                detail = new Equip { Number = valueArray[row, 1].ToString(), TechPos = valueArray[row, 2].ToString() };
                dtLastRep.Id = detail.Id;
                detail.DateLastRep = dtLastRep;
                (detail as Equip).Character = valueArray[row, 4]?.ToString();
                (detail as Equip).Name = valueArray[row, 3]?.ToString();
            }

            if (annualSched)
            {
                detail.FreqRep = valueArray[row, 6].ToString();
                detail.LabRep = Convert.ToDouble(valueArray[row, 9]?.ToString());

                //  detail.DateLastRep.Cur = Convert.ToDateTime(valueArray[row, 7].ToString());
                //  detail.DateLastRep.Cap = Convert.ToDateTime(valueArray[row, 8].ToString());

                detail = new Annual(detail);
                //  ((AnSchedule)detail).YearExc = Convert.ToDateTime(valueArray[row, 26]?.ToString());

                MarkRep mark = new MarkRep();
                mark.Cur = Convert.ToBoolean(valueArray[row, 22]?.ToString());
                mark.Cap = Convert.ToBoolean(valueArray[row, 23]?.ToString());
                mark.Right = Convert.ToBoolean(valueArray[row, 24]?.ToString());
                mark.Tes = Convert.ToBoolean(valueArray[row, 25]?.ToString());
                mark.Id = detail.Id;

                for (int i = 10; i < 22; i++)
                {
                    if (valueArray[row, i] != null)
                    {
                        (detail as Annual).DateRep[i - 10] = valueArray[row, i].ToString();
                    }

                }
            }
            else
            {
                detail.FreqRep = valueArray[row, 5].ToString();
                detail.LabRep = Convert.ToDouble(valueArray[row, 8]?.ToString());
                //  detail.DateLastRep.Cur = Convert.ToDateTime(valueArray[row, 6]);
                //   detail.DateLastRep.Cap = Convert.ToDateTime(valueArray[row, 7].ToString());
                detail = new Mountly(detail);
                for (int i = 9; i < 40; i++)
                {
                    (detail as Mountly).DateRep[i - 9] = valueArray[row, i]?.ToString();
                }
                (detail as Mountly).Note = valueArray[row, 41]?.ToString();
            }


            return detail;
        }
        #endregion
*/
/// <summary>
/// возвращает деталь
/// </summary>
/// <param name="row">номер строки</param>
/// <param name="valueArray">матрица</param>
/// <returns></returns>
        private DetailInfo readDetail2(int row, object[,] valueArray)
        {
            DetailInfo detail = new DetailInfo();

            DateLastRepair dtLastRep = new DateLastRepair { Cur = valueArray[row, 7].ToString(), Cap = valueArray[row, 8].ToString() };
            dtLastRep.Id = detail.Id;
            detail.Number = valueArray[row, 1].ToString();
            detail.TechPos = valueArray[row, 2].ToString();
            detail.DateLastRep = dtLastRep;
            if (_motor)
            {
                detail.TypeDetail = (int)DetailType.Motor;

                string[] str = valueArray[row, 3].ToString().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                double speed = 0;
                double.TryParse(Regex.Split(str[str.Length - 2], "[а-яА-Я]")[0], out speed);
                NameEquip nmEq = new NameEquip { Id = detail.Id, Speed = speed, Type = str[0] };
                detail.Name = nmEq;

                double[] character = getCharacter(valueArray[row, 4].ToString());//.Split(new[] { ' ', "[a - zA - Z] +" }, StringSplitOptions.RemoveEmptyEntries);
                CharacterEquip charEq = new CharacterEquip { Id = detail.Id, Pow = Convert.ToDouble(character[0]), Voltage = Convert.ToDouble(character[1]) };
                detail.Character = charEq;
            }
            else
            {
                detail.TypeDetail = (int)DetailType.Equip;
                NameEquip nmEq = new NameEquip { Id = detail.Id, Name = valueArray[row, 3]?.ToString() };
                detail.Name = nmEq;
                CharacterEquip charEq = new CharacterEquip { Id = detail.Id, Name = valueArray[row, 4]?.ToString() };
                detail.Character = charEq;
            }

            if (_annualSched)
            {
                detail.TypeTime = (int)Time.Annual;

                detail.FreqRep = valueArray[row, 6]?.ToString();
                detail.LabRep = Convert.ToDouble(valueArray[row, 9]?.ToString());

                detail.YearExc = valueArray[row, 26]?.ToString();

                MarkRep mark = new MarkRep();
                mark.Cur = Convert.ToBoolean(valueArray[row, 22]?.ToString());
                mark.Cap = Convert.ToBoolean(valueArray[row, 23]?.ToString());
                mark.Right = Convert.ToBoolean(valueArray[row, 24]?.ToString());
                mark.Tes = Convert.ToBoolean(valueArray[row, 25]?.ToString());
                mark.Id = detail.Id;

                detail.Result = mark;

                detail.DateYear = getDateYear(row, detail.Id, valueArray);
            }
            else
            {
                detail.TypeTime = (int)Time.Mountly;

                detail.FreqRep = valueArray[row, 5].ToString();
                detail.LabRep = Convert.ToDouble(valueArray[row, 8]?.ToString());

                detail.Note = valueArray[row, 41]?.ToString();

                detail.DateMount = getDateMount(row, detail.Id, valueArray);
            }

            return detail;
        }

        private DateYear getDateYear(int row, int idDetail, object[,] valueArray)
        {
            DateYear date = new DateYear();

            date.Id = idDetail;
            date.Jan = valueArray[row, 10]?.ToString();
            date.Feb = valueArray[row, 11]?.ToString();
            date.Mar = valueArray[row, 12]?.ToString();
            date.Apr = valueArray[row, 13]?.ToString();
            date.May = valueArray[row, 14]?.ToString();
            date.Ju = valueArray[row, 15]?.ToString();
            date.Jul = valueArray[row, 16]?.ToString();
            date.Aug = valueArray[row, 17]?.ToString();
            date.Sept = valueArray[row, 18]?.ToString();
            date.Oct = valueArray[row, 19]?.ToString();
            date.Nov = valueArray[row, 20]?.ToString();
            date.Dec = valueArray[row, 21]?.ToString();

            return date;
        }

        private DateMount getDateMount(int row, int idDetail, object[,] valueArray)
        {
            DateMount date = new DateMount();

            date.Id = idDetail;
            date.Day1 = valueArray[row, 9]?.ToString();
            date.Day2 = valueArray[row, 10]?.ToString();
            date.Day3 = valueArray[row, 11]?.ToString();
            date.Day4 = valueArray[row, 12]?.ToString();
            date.Day5 = valueArray[row, 13]?.ToString();
            date.Day6 = valueArray[row, 14]?.ToString();
            date.Day7 = valueArray[row, 15]?.ToString();
            date.Day8 = valueArray[row, 16]?.ToString();
            date.Day9 = valueArray[row, 17]?.ToString();
            date.Day10 = valueArray[row, 18]?.ToString();
            date.Day11 = valueArray[row, 19]?.ToString();
            date.Day12 = valueArray[row, 20]?.ToString();
            date.Day13 = valueArray[row, 21]?.ToString();
            date.Day14 = valueArray[row, 22]?.ToString();
            date.Day15 = valueArray[row, 23]?.ToString();
            date.Day16 = valueArray[row, 24]?.ToString();
            date.Day17 = valueArray[row, 25]?.ToString();
            date.Day18 = valueArray[row, 26]?.ToString();
            date.Day19 = valueArray[row, 27]?.ToString();
            date.Day20 = valueArray[row, 28]?.ToString();
            date.Day21 = valueArray[row, 29]?.ToString();
            date.Day22 = valueArray[row, 30]?.ToString();
            date.Day23 = valueArray[row, 31]?.ToString();
            date.Day24 = valueArray[row, 32]?.ToString();
            date.Day25 = valueArray[row, 33]?.ToString();
            date.Day26 = valueArray[row, 34]?.ToString();
            date.Day27 = valueArray[row, 35]?.ToString();
            date.Day28 = valueArray[row, 36]?.ToString();
            date.Day29 = valueArray[row, 37]?.ToString();
            date.Day30 = valueArray[row, 38]?.ToString();
            date.Day31 = valueArray[row, 39]?.ToString();

            return date;
        }
    }
}
