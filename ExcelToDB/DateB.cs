﻿using ExcelToDB.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ExcelToDB
{
    static class DateB
    {
        /// <summary>
        /// загрузка корпуса в бд
        /// </summary>
        /// <param name="detail"></param>
        static public void addData(Body detail)
        {
            using (DataContext db = new DataContext())
            {
                var bodi = db.Bodies.FirstOrDefault(m => m.Id == detail.Id);
                if (bodi!= null)
                {
                    db.Bodies.Add(detail);
                }
                foreach (DetailInfo det in detail.Details)
                {
                    if (bodi != null)
                    {
                        det.BodyId = bodi.Id;
                    }
                        db.Details.Add(det);

                    db.DateLastRepairs.Add(det.DateLastRep);
                    db.NameEquips.Add(det.Name);
                    db.CharacterEquips.Add(det.Character);

                    if (det.TypeTime == (int)Time.Annual)
                    {
                        db.DateYears.Add(det.DateYear);
                        db.MarkReps.Add(det.Result);
                    }
                    else
                    {
                        db.DateMounts.Add(det.DateMount);

                    }
                }
                db.SaveChanges();
            }
        }

        static public void getData()
        {
            using (DataContext db = new DataContext())
            {
              //  List<DetailInfo> det = db.DetailInfos.Include("DateLastRep").Include("Name").Include("Character").ToList();
            }
        }

        static public bool CheckAddDate(DetailInfo detail)
        {
            DetailInfo detailOld;
            using (DataContext db = new DataContext())
            {
                int korpId = db.Bodies.Where(m => m.Name == detail.Body.Name).Select(m => m.Id).FirstOrDefault();
                var det = db.Details.Include(m => m.DateLastRep).Include(m => m.Name).Include(m => m.Character);
                if (detail.TypeTime == (int)Time.Annual)
                {
                    det = det.Include(m => m.DateYear).Include(m => m.Result);
                }
                else
                {
                    det = det.Include(m => m.DateMount);
                }
                det = det.Where(m => m.Number == detail.Number && m.BodyId == korpId);
                detailOld = det.FirstOrDefault();

            }
            return detailOld == null;

        }
/*
        static public bool EqualsDeatail(DetailInfo oldDetail, DetailInfo newDetail)
        {
        }
        */
    }
}
